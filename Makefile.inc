
# ディレクトリ取得
CURRENT_DIR=$(abspath .)
PARENT_DIR=$(abspath ..)
BASE_DIR:=$(abspath $(dir $(lastword $(MAKEFILE_LIST))))
OUT_DIR=$(BASE_DIR)/video/_output
UTILS_DIR=$(BASE_DIR)/utils/scripts

# 環境
IS_BASH_WSL=$(shell bash -c '[[ "$$(cat /proc/sys/kernel/osrelease)" =~ .*Microsoft.* ]] && echo 1')

# 実行ファイル
AERENDER= $(UTILS_DIR)/sjis_cmd.sh /c/Program\ Files/Adobe/Adobe\ After\ Effects\ CC\ 2017/Support\ Files/aerender.exe
ifeq ($(IS_BASH_WSL),1)
    AERENDER= $(UTILS_DIR)/sjis_cmd.sh /mnt/c/Program\ Files/Adobe/Adobe\ After\ Effects\ CC\ 2017/Support\ Files/aerender.exe
endif
RENDER_OPTS:= -sound ON -OMtemplate LagarithRGBA -mp -continueOnMissingFootage



# コンポ設定
SCENE_NUM=$(shell basename $(PARENT_DIR))
CUT_NUM=$(shell basename $(CURRENT_DIR))
AEP_FILE_NAMES=$(wildcard *.aep)
AEP_FILES=$(addprefix $(CURRENT_DIR)/,$(AEP_FILE_NAMES))
AVI_FILE_NAMES=$(patsubst %.aep,%.avi,$(AEP_FILE_NAMES))
AVI_FILES=$(addprefix $(OUT_DIR)/,$(AVI_FILE_NAMES))
MATERIAL_FILES=$(shell find $(CURRENT_DIR) -iname '*.psd' | sed 's/ /\\ /g')  $(shell find $(CURRENT_DIR) -iname '*.ai' | sed 's/ /\\ /g')
COMP_NAME=_output

#共通処理
define STANDARD_RENDER
    $(AERENDER) -project "$(shell echo "$<" | xargs $(UTILS_DIR)/winpath )" -comp "$(COMP_NAME)" -output "$(shell echo "$@" | xargs $(UTILS_DIR)/winpath)" $(RENDER_OPTS)
endef

# エンコード関連ユーティリティのディレクトリ
UTIL_DIR:=$(BASE_DIR)/utils/bin
ENC_UTIL_DIR:=$(BASE_DIR)/utils/enc_utils/bin

# ユーザー設定
sinclude $(BASE_DIR)/Makefile.user.inc
