
#############################################################################
# マクロ定義
#############################################################################

# 共通マクロ
include Makefile.inc


# サブディレクトリ 
SUBDIRS:=video

# エンコード関連
AVS2WAV:=$(ENC_UTIL_DIR)/avs2wav.exe
FDKAAC:=$(ENC_UTIL_DIR)/fdkaac.exe
X264:=$(ENC_UTIL_DIR)/x264_x64.exe
MUXER:=$(ENC_UTIL_DIR)/muxer.exe
REMUXER:=$(ENC_UTIL_DIR)/remuxer.exe

AOPTION=-b 192000 --profile 2
AOPTION_ECO=-b 64000 --profile 29

VOPTION_COMMON=--opencl --preset slower --me umh --bframes 3 --keyint 240 --min-keyint 23 --qcomp 0.70 --rc-lookahead 240 --scenecut 65 --no-dct-decimate --no-fast-pskip --aq-mode 1 --aq-strength 0.8
	# 注： bframesは3より大きいと黒背景白文字のスライドの動き（EDロールなど）で破綻する
	#      scenecutはアニメではデフォルトより大きめにしておくと良いらしい

VOPTION=     $(VOPTION_COMMON) --crf 12  --vbv-maxrate 40000 --vbv-bufsize 30000 --profile high --partitions all --video-filter resize:1280,720,1:1:lanczos --transfer "bt709" --colormatrix "bt709"
VOPTION_BD=  $(VOPTION_COMMON) --crf 16  --bluray-compat --vbv-maxrate 40000 --vbv-bufsize 30000 --level 4.1 --keyint 24 --slices 4 --colorprim "bt709" --transfer "bt709" --colormatrix "bt709" --sar 1:1 --b-pyramid strict --weightp 0
VOPTION_ECO= $(VOPTION_COMMON) --bitrate 230  --profile main --8x8dct --partitions "p8x8,b8x8,i8x8,i4x4" --video-filter resize:352,200,1:1:lanczos
VOPT_FAST=--analyse none --subme 1 --me dia
VOPT_SLOW=
FPS=24

PART1_IN_AVS=main_part1.avs
VER_AVS=version.avs
PART1_OUT_MP4=_release/kosys_ep03_part1.mp4
PART1_TMPA1=_release/kosys_ep03_part1.tmp.m4a
PART1_TMPV1=_release/kosys_ep03_part1.tmp.264
PART1_TMPV2=_release/kosys_ep03_part1.tmp.mp4
PART1_OUT_MP4_PRE=_release/kosys_ep03_part1_pre.mp4
PART1_TMPV1_PRE=_release/kosys_ep03_part1_pre.tmp.264
PART1_TMPV2_PRE=_release/kosys_ep03_part1_pre.tmp.mp4
PART1_OUT_MP4_BD=_release/kosys_ep03_part1_bd.mp4
PART1_TMPV1_BD=_release/kosys_ep03_part1_bd.tmp.264
PART1_TMPV2_BD=_release/kosys_ep03_part1_bd.tmp.mp4
PART1_OUT_MP4_ECO=_release/kosys_ep03_part1_eco.mp4
PART1_TMPA1_ECO=_release/kkosys_ep03_part1_eco.tmp.m4a
PART1_TMPV1_ECO=_release/kosys_ep03_part1_eco.tmp.264
PART1_TMPV2_ECO=_release/kosys_ep03_part1_eco.tmp.mp4

#############################################################################
# Makeルール
#############################################################################

.PHONY: all $(SUBDIRS)
all: $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@


.PHONY: part1
part1: version $(PART1_OUT_MP4)
$(PART1_OUT_MP4): $(PART1_TMPA1) $(PART1_TMPV1) $(PART1_TMPV2)
	$(REMUXER) -i $(PART1_TMPV2) -i $(PART1_TMPA1) -o $(PART1_OUT_MP4)

$(PART1_TMPV2): $(PART1_TMPV1)
	$(MUXER) -i $(PART1_TMPV1)?fps=$(FPS) -o $(PART1_TMPV2)


$(PART1_TMPV1): $(wildcard *.avs) $(wildcard video/*.avs) $(wildcard video/_output/*.avi) $(VER_AVS)
	$(X264) $(VOPTION) $(VOPT_SLOW) -o $(PART1_TMPV1) $(PART1_IN_AVS)
	

$(PART1_TMPA1): $(wildcard audio/*.avs) $(wildcard audio/*.wav) $(wildcard audio/*.mp3) $(wildcard audio/*.aac) $(wildcard audio/*.m4a)
	$(AVS2WAV) $(PART1_IN_AVS)  - |$(FDKAAC) $(AOPTION) -o $(PART1_TMPA1) -
	

.PHONY: part1-preview
part1-preview: version $(PART1_OUT_MP4_PRE)
$(PART1_OUT_MP4_PRE): $(PART1_TMPA1) $(PART1_TMPV1_PRE) $(PART1_TMPV2_PRE)
	$(REMUXER) -i $(PART1_TMPV2_PRE) -i $(PART1_TMPA1) -o $(PART1_OUT_MP4_PRE)

$(PART1_TMPV2_PRE): $(PART1_TMPV1_PRE)
	$(MUXER) -i $(PART1_TMPV1_PRE)?fps=$(FPS) -o $(PART1_TMPV2_PRE)

$(PART1_TMPV1_PRE): $(wildcard *.avs) $(wildcard video/*.avs) $(wildcard video/_output/*.avi) $(VER_AVS)
	$(X264) $(VOPTION) $(VOPT_FAST) -o $(PART1_TMPV1_PRE) $(PART1_IN_AVS)
	

part1-eco: version $(PART1_OUT_MP4_ECO)
$(PART1_OUT_MP4_ECO): $(PART1_OUT_MP4) $(PART1_TMPA1_ECO)
	$(X264) $(VOPTION_ECO) --pass 1 $(VOPT_FAST)  -o $(PART1_TMPV1_ECO) $(PART1_OUT_MP4)
	$(X264) $(VOPTION_ECO) --pass 2 $(VOPT_SLOW)  -o $(PART1_TMPV1_ECO) $(PART1_OUT_MP4)
	$(MUXER) -i $(PART1_TMPV1_ECO)?fps=$(FPS) -o $(PART1_TMPV2_ECO)
	$(REMUXER) -i $(PART1_TMPV2_ECO) -i $(PART1_TMPA1_ECO) -o $(PART1_OUT_MP4_ECO)
	
$(PART1_TMPA1_ECO): $(wildcard audio/*.avs) $(wildcard audio/*.wav) $(wildcard audio/*.mp3) $(wildcard audio/*.aac) $(wildcard audio/*.m4a)
	$(AVS2WAV) $(PART1_IN_AVS)  - |$(FDKAAC) $(AOPTION_ECO) -o $(PART1_TMPA1_ECO) -


.PHONY: part1-bd
part1-bd: version $(PART1_OUT_MP4_BD)
$(PART1_OUT_MP4_BD): $(PART1_TMPA1) $(PART1_TMPV1_BD) $(PART1_TMPV2_BD)
	$(REMUXER) -i $(PART1_TMPV2_BD) -i $(PART1_TMPA1) -o $(PART1_OUT_MP4_BD)

$(PART1_TMPV2_BD): $(PART1_TMPV1_BD)
	$(MUXER) -i $(PART1_TMPV1_BD)?fps=$(FPS) -o $(PART1_TMPV2_BD)

$(PART1_TMPV1_BD): $(wildcard *.avs) $(wildcard video/*.avs) $(wildcard video/_output/*.avi) $(VER_AVS)
	$(X264) $(VOPTION_BD) $(VOPT_SLOW) -o $(PART1_TMPV1_BD) $(PART1_IN_AVS)
	

.PHONY: version
version:
# gitのリビジョン番号を取得する
	echo 'global VERSION="$(shell git describe --abbrev=7 --dirty --always --tags)"' >  $(VER_AVS).tmp
# リビジョン番号が変化していたときのみ上書きする
	test -e $(VER_AVS) || cp -f $(VER_AVS).tmp $(VER_AVS)
	test "$$(md5sum $(VER_AVS).tmp | awk '{ print $$1 }')" != "$$(md5sum $(VER_AVS) | awk '{ print $$1 }')" && cp -f $(VER_AVS).tmp $(VER_AVS) || true
	rm -f $(VER_AVS).tmp


.PHONY: thumbnails
thumbnails: $(patsubst video/_output/%.avi,_release/img/%.jpg,$(wildcard video/_output/*.avi))
_release/img/%.jpg: $(OUT_DIR)/%.avi
	mkdir -p _release/img
	ffmpeg -i "$<" -vframes 1 -ss 2.0 -f image2 -s 640x360 -y "$@" || true
	test -f "$@" || ffmpeg -i "$<" -vframes 1 -ss 0 -f image2 -s 640x360 -y "$@"


.PHONY: authors-html
authors-html: thumbnails
	$(BASE_DIR)/utils/scripts/make_authors_html_groupby_cut.sh
